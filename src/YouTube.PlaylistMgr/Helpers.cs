﻿using System;
using FirstFloor.ModernUI.Presentation;
using YouTube.PlaylistMgr.Service;
using Playlist = Google.Apis.Youtube.v3.Data.Playlist;

namespace YouTube.PlaylistMgr
{
    public static class Helpers
    {
        public static Uri Uri(this Playlist plst, string token)
        {
            return new Uri(string.Format("cmd://{0}/playlist?{1}?{2}",Guid.NewGuid(),token,plst.Id),UriKind.Absolute);
        }

        public static Uri NewUri(this Uri uri)
        {
            string[] parts = uri.Segments;
            return new Uri(string.Format("{0}://{1}{2}", uri.Scheme, Guid.NewGuid(), uri.PathAndQuery));
        }

        public static bool IsLinkFor(this Link link, string token, string playListId)
        {
            return link.Source.OriginalString.Contains(string.Format("?{0}?{1}", token, playListId));
        }
    }
}
