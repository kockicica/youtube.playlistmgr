﻿using System.Collections.Generic;

namespace YouTube.PlaylistMgr.Service
{
    public interface ITokenStore
    {
        void Store(string token);
        void Delete(string token);
        IEnumerable<string> GetAll();
    }
}
