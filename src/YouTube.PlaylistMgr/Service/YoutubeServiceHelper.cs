﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotNetOpenAuth.OAuth2;
using Google;
using Google.Apis.Authentication;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Plus.v1;
using Google.Apis.Util;
using Google.Apis.Youtube.v3;
using Google.Apis.Youtube.v3.Data;
using log4net;

namespace YouTube.PlaylistMgr.Service
{
    public class YoutubeServiceHelper
    {
        private IAuthenticator _auth;
        private NativeApplicationClient _provider;
        private static ILog _logger = LogManager.GetLogger(typeof (YoutubeServiceHelper));

        private string _refreshToken;

        public YoutubeServiceHelper(string refreshToken)
        {
            _refreshToken = refreshToken;
            _provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) { ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret };
            _auth = new OAuth2Authenticator<NativeApplicationClient>(_provider, client =>
            {
                IAuthorizationState state = new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
                state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                state.RefreshToken = _refreshToken;
                client.RefreshToken(state);
                return state;
            });

        }

        public Task<IEnumerable<Google.Apis.Youtube.v3.Data.Playlist>> GetPlayLists(string token)
        {
            var tcs = new TaskCompletionSource<IEnumerable<Google.Apis.Youtube.v3.Data.Playlist>>();

            new Thread(() =>
            {
//                var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) { ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret };
//                var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, client =>
//                {
//                    IAuthorizationState state = new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
//                    state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
//                    state.RefreshToken = token;
//                    client.RefreshToken(state);
//                    return state;
//                });

                var youtube = new YoutubeService(_auth);
                var listRequest = youtube.Playlists.List("snippet");
                listRequest.Mine = true;

                var listRsp = listRequest.Fetch();
                tcs.SetResult(listRsp.Items);
            }).Start();

            return tcs.Task;
        }

        public Task<IEnumerable<PlaylistItem>> GetPlayList(string playListId)
        {
            var tcs = new TaskCompletionSource<IEnumerable<PlaylistItem>>();

            new Thread(() =>
            {
                var ret = new List<PlaylistItem>();
//                var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) { ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret };
//                var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, client =>
//                {
//                    IAuthorizationState state = new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
//                    state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
//                    state.RefreshToken = token;
//                    client.RefreshToken(state);
//                    return state;
//                });

                var youtube = new YoutubeService(_auth);
                var listRequest = youtube.PlaylistItems.List("snippet");
                listRequest.PlaylistId = playListId;
                listRequest.MaxResults = 50;
                string nextPageToken = null;
                do
                {
                    if (!string.IsNullOrEmpty(nextPageToken))
                        listRequest.PageToken = nextPageToken;
                    var listRsp = listRequest.Fetch();
                    ret.AddRange(listRsp.Items.Select(playlist =>
                    {
                        return new PlaylistItem
                        {
                            Description = playlist.Snippet.Description,
                            VideoId = playlist.Snippet.ResourceId.VideoId,
                            Title = playlist.Snippet.Title,
                            Thumbnail = playlist.Snippet.Thumbnails["default"].Url,
                            PlaylistId = playListId,
                            ItemId = playlist.Id
                        };
                    }));
                    nextPageToken = listRsp.NextPageToken;

                } while (!string.IsNullOrEmpty(nextPageToken));
                tcs.SetResult(ret);

            }).Start();

            return tcs.Task;
        }

        public void CopyVideoToPlayList(string targetPlayListId, Service.PlaylistItem item, bool move = false)
        {
            var youtube = new YoutubeService(_auth);
            var pls = new Google.Apis.Youtube.v3.Data.PlaylistItem
            {
                Snippet = new PlaylistItemSnippet
                {
                    PlaylistId = targetPlayListId,
                    ResourceId = new ResourceId { Kind = "youtube#video", VideoId = item.VideoId }
                }
            };

            var listRequest = youtube.PlaylistItems.Insert(pls, "snippet");
            try
            {
                _logger.DebugFormat("Copying video id:{0} from playlist:{1} to playlist:{2}", item.VideoId, item.PlaylistId, targetPlayListId);
                var listRsp = listRequest.Fetch();
                if (move)
                {
                    _logger.DebugFormat("Deleting video id:{0} item id:{1} from playlist:{2}", item.VideoId, item.ItemId, item.PlaylistId);
                    var deleteReq = youtube.PlaylistItems.Delete(item.ItemId);
                    var deleteRsp = deleteReq.Fetch();
                }
            }
            catch (GoogleApiRequestException e)
            {
                _logger.Warn(e);
                throw;
            }

        }

        public void DeletePlaylistItem(Service.PlaylistItem playlistItem)
        {
            try
            {
                var youtube = new YoutubeService(_auth);
                _logger.DebugFormat("Deleting playlist item id:{0}", playlistItem.ItemId);
                var deleteReq = youtube.PlaylistItems.Delete(playlistItem.ItemId);
                var deleteRsp = deleteReq.Fetch();
            }
            catch (GoogleApiException e)
            {
                _logger.Warn(e);
                throw;
            }
        }

    }
}
