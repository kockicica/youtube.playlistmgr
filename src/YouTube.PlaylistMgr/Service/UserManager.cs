﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Plus.v1;
using Google.Apis.Util;
using Google.Apis.Youtube.v3;
using log4net;

namespace YouTube.PlaylistMgr.Service
{
    public class UserManager: IUserManager
    {
        private static ILog logger = LogManager.GetLogger(typeof (UserManager));

        private ITokenStore _tokenStore;
        private IGoogleAuthorize _googleAuthorize;

        public UserManager(ITokenStore tokenStore, IGoogleAuthorize googleAuthorize)
        {
            _tokenStore = tokenStore;
            _googleAuthorize = googleAuthorize;
        }

        public IEnumerable<User> GetUsers()
        {
            var tokens = _tokenStore.GetAll();
            return tokens.Select(GetUser).ToList();
        }

        public bool DeleteUser(User user)
        {
            throw new NotImplementedException();
        }

        public void CreateUser()
        {
            string token = string.Empty;
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) { ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret };
            var auth = new OAuth2Authenticator<NativeApplicationClient>(provider,
                                                                        client =>
                                                                        {
                                                                            IAuthorizationState state =
                                                                                new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
                                                                            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                                                                            Uri authUri = client.RequestUserAuthorization(state);
                                                                            string authCode = _googleAuthorize.Authorize(authUri);
                                                                            var ath = client.ProcessUserAuthorization(authCode, state);
                                                                            token = ath.RefreshToken;
                                                                            //_tokenStore.Store(ath.RefreshToken);
                                                                            return ath;
                                                                        });
            var gplus = new Google.Apis.Plus.v1.PlusService(auth);
            var plusReq = gplus.People.Get("me");
            var plusRsp = plusReq.Fetch();
            _tokenStore.Store(token);

        }

        public Task CreateUserAsync()
        {
            return Task.Run(() =>
                {
                    try
                    {
                        string token = string.Empty;
                        var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) { ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret };
                        var auth = new OAuth2Authenticator<NativeApplicationClient>(provider,
                                                                                    client =>
                                                                                        {
                                                                                            IAuthorizationState state =
                                                                                                new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
                                                                                            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                                                                                            Uri authUri = client.RequestUserAuthorization(state);
                                                                                            string authCode = _googleAuthorize.Authorize(authUri);
                                                                                            var ath = client.ProcessUserAuthorization(authCode, state);
                                                                                            token = ath.RefreshToken;
                                                                                            //_tokenStore.Store(ath.RefreshToken);
                                                                                            return ath;
                                                                                        });
                        var gplus = new PlusService(auth);
                        var plusReq = gplus.People.Get("me");
                        var plusRsp = plusReq.Fetch();
                        _tokenStore.Store(token);
                    }
                    catch (Exception e)
                    {
                        logger.Error(e);
                    }                    
                });
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            var ret = new List<User>();
            var tokens = _tokenStore.GetAll().Where(s => !string.IsNullOrEmpty(s));
            foreach (var token in tokens)
            {
                var user = await GetUserAsync(token);
                ret.Add(user);
            }
            return ret;

        }

        private User GetUser(string token)
        {
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) {ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret};
            var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, client =>
                {
                    IAuthorizationState state = new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
                    state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                    state.RefreshToken = token;
                    client.RefreshToken(state);
                    return state;
                    
                });

            var gplus = new Google.Apis.Plus.v1.PlusService(auth);
            var plusReq = gplus.People.Get("me");
            var plusRsp = plusReq.Fetch();

            return new User {Id = plusRsp.Id, Image = plusRsp.Image.Url, FamilyName = plusRsp.Name.FamilyName,GivenName = plusRsp.Name.GivenName,RefreshToken = token};


        }

        private Task<User> GetUserAsync(string token)
        {

            var tcs = new TaskCompletionSource<User>();

            new Thread(() =>
                {
                    try
                    {
                        var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description) { ClientIdentifier = Client.ClientIdentifier, ClientSecret = Client.ClientSecret };
                        var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, client =>
                        {
                            IAuthorizationState state = new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.GetStringValue(), PlusService.Scopes.PlusMe.GetStringValue() });
                            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                            state.RefreshToken = token;
                            client.RefreshToken(state);
                            return state;

                        });
                        var gplus = new PlusService(auth);
                        var plusReq = gplus.People.Get("me");
                        var plusRsp = plusReq.Fetch();
                        tcs.SetResult(new User { Id = plusRsp.Id, Image = plusRsp.Image.Url, FamilyName = plusRsp.Name.FamilyName, GivenName = plusRsp.Name.GivenName, RefreshToken = token, DisplayName = plusRsp.DisplayName});
                    }
                    catch (Exception e)
                    {
                        logger.Error(e);
                    }
                }).Start();

            return tcs.Task;

        }


    }
}
