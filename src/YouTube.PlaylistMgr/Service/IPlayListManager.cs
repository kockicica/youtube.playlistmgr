﻿using System.Collections.Generic;

namespace YouTube.PlaylistMgr.Service
{
    public interface IPlayListManager
    {
        IEnumerable<Playlist> GetPersonalPlayLists();
    }
}
