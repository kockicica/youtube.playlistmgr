﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace YouTube.PlaylistMgr.Service
{
    public interface IUserManager
    {
        IEnumerable<User> GetUsers();
        bool DeleteUser(User user);
        void CreateUser();
        Task CreateUserAsync();

        Task<IEnumerable<User>> GetUsersAsync();

    }
}
