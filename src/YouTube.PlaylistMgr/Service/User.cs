﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using YouTube.PlaylistMgr.Properties;

namespace YouTube.PlaylistMgr.Service
{
    public class User: DependencyObject,INotifyPropertyChanged
    {

        private string _familyName;
        private string _image;
        private string _givenName;
        private string _displayName;
         

        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                _displayName = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("DisplayName"));
            }
        }
 

        public string GivenName
        {
            get { return _givenName; }
            set
            {
                _givenName = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("GivenName"));
            }
        }


        public string Id { get; set; }
        public string RefreshToken { get; set; }
        
        //public static readonly DependencyProperty RefreshTokenProperty = DependencyProperty.Register("RefreshToken", typeof (string), typeof (User), new PropertyMetadata(""));
         

//        public string RefreshToken
//        {
//            get { return (string) GetValue(RefreshTokenProperty); }
//            set { SetValue(RefreshTokenProperty, value); }
//        }

        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Image"));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public string FamilyName
        {
            get { return _familyName; }
            set
            {
                _familyName = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
