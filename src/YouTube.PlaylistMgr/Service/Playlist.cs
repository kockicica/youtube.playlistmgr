﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;
using YouTube.PlaylistMgr.Annotations;

namespace YouTube.PlaylistMgr.Service
{
    [Serializable]
    //[XmlInclude(typeof(PlaylistItem))]
    public class Playlist
    {
        public virtual string Id { get; set; }
        public virtual string ETag { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual IList<PlaylistItem> Items { get; set; }

        public Playlist()
        {
            Items = new List<PlaylistItem>();
        }
    }

    public class PlaylistItem: INotifyPropertyChanged
    {
        private string _videoId;
        private string _itemId;
        private string _title;
        private string _description;
        private string _thumbnail;
        private string _playlistId;
        private bool _isInCut;

        public virtual string VideoId   
        {
            get { return _videoId; }
            set
            {
                _videoId = value;
                OnPropertyChanged();
            }
        }

        public virtual string ETag { get; set; }
        
        public virtual string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public virtual string Thumbnail 
        {
            get { return _thumbnail; }
            set
            {
                _thumbnail = value;
                OnPropertyChanged();
            }
        }

        public virtual string PlaylistId
        {
            get { return _playlistId; }
            set
            {
                _playlistId = value;
                OnPropertyChanged();
            }
        }

        public virtual string ItemId
        {
            get { return _itemId; }
            set
            {
                _itemId = value;
                OnPropertyChanged();
            }
        }

        public bool IsInCut
        {
            get { return _isInCut; }
            set
            {
                _isInCut = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public static class AttachedProperties
    {
        public static readonly DependencyProperty IsInCutOperationProperty = DependencyProperty.RegisterAttached("IsInCutOperation",
                                                                                                                 typeof (bool),
                                                                                                                 typeof (ListBoxItem),
                                                                                                                 new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        public static void SetIsInCutOperation(FrameworkElement element, bool value)
        {
            element.SetValue(IsInCutOperationProperty, value);
        }

        public static bool GetIsInCutOperation(FrameworkElement element)
        {
            return (bool) element.GetValue(IsInCutOperationProperty);
        }
    }
}
