﻿using System;

namespace YouTube.PlaylistMgr.Service
{
    public interface IGoogleAuthorize
    {
        string Authorize(Uri authUri);
    }
}
