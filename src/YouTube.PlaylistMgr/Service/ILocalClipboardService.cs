﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace YouTube.PlaylistMgr.Service
{
    public interface ILocalClipboardService
    {
        ICollection<PlaylistItem> ItemsInCut { get; }
    }
}