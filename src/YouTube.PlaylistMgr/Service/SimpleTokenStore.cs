﻿using System.Collections.Generic;
using System.IO;
using log4net;

namespace YouTube.PlaylistMgr.Service
{
    public class SimpleTokenStore: ITokenStore
    {
        private static ILog logger = LogManager.GetLogger(typeof (SimpleTokenStore));

        private string _fileName;

        public SimpleTokenStore(string fileName)
        {
            _fileName = fileName;
        }

        public void Store(string token)
        {
            var tokens = ReadAllTokens(_fileName);
            if (!tokens.Contains(token))
                tokens.Add(token);
            File.WriteAllLines(_fileName, tokens);
        }


        public IEnumerable<string> GetAll()
        {
            return ReadAllTokens(_fileName);
        }

        public void Delete(string token)
        {
            var tokens = ReadAllTokens(_fileName);
            if (tokens.Contains(token))
                tokens.Remove(token);
            File.WriteAllLines(_fileName, tokens);
        }

        private IList<string> ReadAllTokens(string file)
        {
            if (File.Exists(file))
                return new List<string>(File.ReadAllLines(file));
            return new List<string>();
        }
    }
}
