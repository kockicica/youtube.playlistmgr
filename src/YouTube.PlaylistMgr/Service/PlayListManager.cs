﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;

namespace YouTube.PlaylistMgr.Service
{
    public class PlayListManager: IPlayListManager
    {
        private ITokenStore _tokenStore;

        public PlayListManager(ITokenStore tokenStore)
        {
            _tokenStore = tokenStore;
        }

        public IEnumerable<Playlist> GetPersonalPlayLists()
        {
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
            provider.ClientIdentifier = "709850455731-ook2tnco8gjp3766tuktjk9m4m78t8f4.apps.googleusercontent.com";
            provider.ClientSecret = "UrJT_zxAe8kfqzfWZD_Zn7e6";
            var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthorization);

            throw new NotImplementedException();
        }

        private IAuthorizationState GetAuthorization(NativeApplicationClient arg)
        {
            // Get the auth URL:
            //            IAuthorizationState state = new AuthorizationState(new[] { YoutubeService.Scopes.Youtube.ToString() });

            IAuthorizationState state = new AuthorizationState(new[] { "https://www.googleapis.com/auth/youtube" });
            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);

            var refreshToken = string.Empty;

            if (string.IsNullOrEmpty(refreshToken))
            {
                Uri authUri = arg.RequestUserAuthorization(state);

                // Request authorization from the user (by opening a browser window):
                Process.Start(authUri.ToString());
                Console.Write("  Authorization Code: ");
                string authCode = Console.ReadLine();
                Console.WriteLine();

                // Retrieve the access token by using the authorization code:
                var auth = arg.ProcessUserAuthorization(authCode, state);

                _tokenStore.Store(auth.RefreshToken);


                return auth;
            }
            else
            {
                state.RefreshToken = refreshToken;
                arg.RefreshToken(state);
                Console.WriteLine(state.AccessToken);
                return state;
            }


        }

        private string GetRefreshToken()
        {
            string fileName = "refreshtoken";
            if (File.Exists(fileName))
            {
                var file = new StreamReader("refreshtoken");
                return file.ReadLine();
            }
            return string.Empty;
        }

        private void WriteRefreshToken(string token)
        {
            string fileName = "refreshtoken";
            var file = new StreamWriter(fileName);
            file.Write(token);
            file.Close();
        }


    }
}
