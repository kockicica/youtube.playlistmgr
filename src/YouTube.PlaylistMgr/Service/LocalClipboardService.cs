﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.PlaylistMgr.Service
{
    public class ClearableObservableCollection<T> : ObservableCollection<T>
    {
        public event EventHandler<EventArgs> Clearing;

        protected virtual void OnClearing()
        {
            EventHandler<EventArgs> handler = Clearing;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected override void ClearItems()
        {
            OnClearing();
            base.ClearItems();
        }
    }

    public class LocalClipboardService : ILocalClipboardService
    {
        private ClearableObservableCollection<PlaylistItem> _itemsInCut;
        
        public ICollection<PlaylistItem> ItemsInCut
        {
            get { return _itemsInCut; }
        }

        public LocalClipboardService()
        {
            _itemsInCut = new ClearableObservableCollection<PlaylistItem>();
            _itemsInCut.CollectionChanged += (sender, args) =>
                {
                    switch (args.Action)
                    {
                        case NotifyCollectionChangedAction.Add:
                            foreach (PlaylistItem newItem in args.NewItems)
                            {
                                newItem.IsInCut = true;
                            }
                            break;
                        case NotifyCollectionChangedAction.Remove:
                            foreach (PlaylistItem newItem in args.OldItems)
                            {
                                newItem.IsInCut = false;
                            }
                            break;
                    }
                };
            _itemsInCut.Clearing += (sender, args) =>
                {
                    foreach (var playlistItem in ItemsInCut)
                    {
                        playlistItem.IsInCut = false;
                    }
                };
        }
    }
}
