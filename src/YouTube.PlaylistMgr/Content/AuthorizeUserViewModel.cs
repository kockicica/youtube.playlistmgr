﻿using FirstFloor.ModernUI.Presentation;

namespace YouTube.PlaylistMgr.Content
{
    public class AuthorizeUserViewModel: NotifyPropertyChanged
    {
        private string _name;
        private string _url;
        private string _result;


        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                OnPropertyChanged("Url");
            }
        }

        public string Result
        {
            get { return _result; }
            set
            {
                _result = value;
                OnPropertyChanged("Result");
            }
        }
    }
}
