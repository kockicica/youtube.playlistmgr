﻿using System.Windows.Controls;
using System.Windows.Input;
using mshtml;

namespace YouTube.PlaylistMgr.Content
{
    /// <summary>
    /// Interaction logic for AuthorizeUser.xaml
    /// </summary>
    public partial class AuthorizeUser : UserControl
    {
        public AuthorizeUser(int all)
        {
            InitializeComponent();

            WebBrowser.LoadCompleted += (sender, args) =>
            {
                var doc = (HTMLDocument)WebBrowser.Document;
                var titles = doc.getElementsByTagName("title");
                int a = titles.length;
                IHTMLElement item = titles.item(null, 0);
                if (item != null)
                {
                    var parts = item.innerText.Split('=');
                    if (parts.Length == 2 && parts[0] == "Success code")
                        ((AuthorizeUserViewModel)DataContext).Result = parts[1];
                    else
                        ((AuthorizeUserViewModel)DataContext).Result = item.innerText;
                }

            };
        }

        private void CommandBinding_OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

    }
}
