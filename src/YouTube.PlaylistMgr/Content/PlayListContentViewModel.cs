﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using YouTube.PlaylistMgr.Commands;
using YouTube.PlaylistMgr.Pages;
using YouTube.PlaylistMgr.Service;

namespace YouTube.PlaylistMgr.Content
{
    public class PlayListContentViewModel
    {
        public ObservableCollection<PlaylistItem> Items { get; set; }
        public Playlist Playlist { get; set; }
        public string RefreshToken { get; set; }
        public YoutubeServiceHelper YoutubeServiceHelper { get; set; }
        public PlayListContent View { get; set; }

        private ICommand _deleteSelectedCommand;
        private ICommand _copySelectedCommand;
        private ICommand _cutSelectedCommand;
        private ICommand _pasteCommand;
        private ILocalClipboardService _clipboardService;

        public PlayListContentViewModel(PlayListContent view, ILocalClipboardService clipboardService)
        {
            View = view;
            _clipboardService = clipboardService;

            _deleteSelectedCommand = new DeletePlaylistItemsCommand(this);
            _copySelectedCommand = new CopyPlaylistItemsCommand(_clipboardService,this);
            _cutSelectedCommand = new CutPlaylistItemsCommand(_clipboardService,this);
            _pasteCommand = new PastePlaylistItemsCommand(_clipboardService, this);
        }

        public ICommand DeleteSelectedCommand
        {
            get { return _deleteSelectedCommand; }
        }

        public ICommand CopySelectedCommand
        {
            get { return _copySelectedCommand; }
        }

        public ICommand PasteCommand
        {
            get { return _pasteCommand; }
        }

        public ICommand CutSelectedCommand
        {
            get { return _cutSelectedCommand; }
        }
    }
}
