﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using FirstFloor.ModernUI.Windows.Controls;
using YouTube.PlaylistMgr.Service;

namespace YouTube.PlaylistMgr.Content
{
    /// <summary>
    /// Interaction logic for PlayListContent.xaml
    /// </summary>
    public partial class PlayListContent : UserControl
    {
        public PlayListContent()
        {
            InitializeComponent();


//            Videos.MouseMove += (sender, args) =>
//                {
//                    if (args.LeftButton == MouseButtonState.Pressed)
//                    {
//                        ListBox listbox = sender as ListBox;
//                        if (listbox != null && listbox.SelectedItems != null && listbox.SelectedItems.Count > 0)
//                        {
//                            var selectedItems = new List<PlaylistItem>(listbox.SelectedItems.Cast<PlaylistItem>());
//                            var data = new DataObject("playlistitem", selectedItems);
//                            DragDrop.DoDragDrop(listbox, data, DragDropEffects.Copy | DragDropEffects.Move);
//                        }
//                    }
//
//                };
//
//            Videos.DragOver += ItemsList_DragOver;
//
//            Videos.Drop += (sender, args) =>
//                {
//                    if (args.Handled == false)
//                    {
//                        IList<PlaylistItem> droppedItems = null;
//                        if (args.Data.GetDataPresent("playlistitem"))
//                        {
//                            droppedItems = args.Data.GetData("playlistitem") as IList<PlaylistItem>;
//                        }
//
//
//                        var droppedOnItem = ((ListBox) sender).InputHitTest(args.GetPosition((ListBox) sender)) as FrameworkElement;
//                        if (droppedOnItem != null)
//                        {
//                            PlaylistItem item = null;
//                            var parent = droppedOnItem.TemplatedParent as ContentPresenter;
//                            if (parent != null)
//                            {
//                                item = parent.Content as PlaylistItem;
//                            }
//                            if (item != null && droppedItems != null && droppedItems.Count > 0)
//                            {
//                                ModernDialog.ShowMessage(string.Format("Insert {0} on place of the {1}", droppedItems[0].Title,item.Title), "Message", MessageBoxButton.OK);
//                            }
//                        }
//                        args.Handled = true;
//                    }
//                };

        }


//        private void ItemsList_DragOver(object sender, System.Windows.DragEventArgs e)
//        {
//            ListBox li = sender as ListBox;
//            ScrollViewer sv = FindVisualChild<ScrollViewer>(Videos);
//
//            double tolerance = 10;
//            double verticalPos = e.GetPosition(li).Y;
//            double offset = 3;
//
//            if (verticalPos < tolerance) // Top of visible list?
//            {
//                sv.ScrollToVerticalOffset(sv.VerticalOffset - offset); //Scroll up.
//            }
//            else if (verticalPos > li.ActualHeight - tolerance) //Bottom of visible list?
//            {
//                sv.ScrollToVerticalOffset(sv.VerticalOffset + offset); //Scroll down.    
//            }
//        }
//
//        public static TChildItem FindVisualChild<TChildItem>(DependencyObject obj) where TChildItem : DependencyObject
//        {
//            // Search immediate children first (breadth-first)
//            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
//            {
//                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
//
//                if (child != null && child is TChildItem)
//                    return (TChildItem)child;
//
//                else
//                {
//                    TChildItem childOfChild = FindVisualChild<TChildItem>(child);
//
//                    if (childOfChild != null)
//                        return childOfChild;
//                }
//            }
//
//            return null;
//        }


        private void CommandBinding_CutExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ((PlayListContentViewModel) DataContext).CutSelectedCommand.Execute(Videos.SelectedItems);
        }

        private void CommandBinding_CopyExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ((PlayListContentViewModel)DataContext).CopySelectedCommand.Execute(Videos.SelectedItems);
        }

        private void CommandBinding_PasteExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ((PlayListContentViewModel)DataContext).PasteCommand.Execute(Videos.SelectedItems);
        }

    }
}
