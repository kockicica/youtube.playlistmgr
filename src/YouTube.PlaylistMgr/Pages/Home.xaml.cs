﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;
using ModernUIApp1;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;
using log4net;

namespace YouTube.PlaylistMgr.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl, IGoogleAuthorize
    {
        private static ILog logger = LogManager.GetLogger(typeof (Home));

        private IUserManager _userManager;
        private ITokenStore _tokenStore;

        public Home()
        {
            InitializeComponent();

            _tokenStore = new SimpleTokenStore("tokenstore");
            _userManager = new UserManager(_tokenStore, this);

            FillData();

            TestButton.Click += (sender, args) =>
                {
                    try
                    {
                        _userManager.CreateUser();
                        FillData();
                    }
                    catch (Exception e)
                    {
                        logger.Error(e);
                    }
                };

        }

        public async void FillData()
        {
            LoadingProgressBar.Visibility = Visibility.Visible;
            UsersList.Visibility = Visibility.Hidden;
            var users = await _userManager.GetUsersAsync();
            DataContext = new ObservableCollection<User>(users);
            LoadingProgressBar.Visibility = Visibility.Hidden;
            UsersList.Visibility = Visibility.Visible;
        }

        public string Authorize(Uri authUri)
        {
            var content = new AuthorizeUser(11);
            var viewModel = new AuthorizeUserViewModel() {Name = "Login", Url = authUri.ToString()};

            var dlg = new ModernDialog()
            {
                Title = "Authorize",
                Content = content,
                DataContext = viewModel
            };
            content.WebBrowser.Navigate(viewModel.Url);
            dlg.ShowDialog();

            return viewModel.Result;
        }

        private void UsersList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var frame = NavigationHelper.FindFrame(null, this);
            if (frame != null)
            {
                var user = (User)UsersList.SelectedItem;

                var uri = new Uri(string.Format("/Pages/PlayListManager.xaml#{0}", user.Id), UriKind.Relative);

                var wnd = Window.GetWindow(this) as MainWindow;

                var link = new Link { DisplayName = user.DisplayName, Source = uri };
                if (!wnd.HomeLinkGroup.Links.Any(link1 => link1.Source == uri))
                    wnd.HomeLinkGroup.Links.Add(link);

                frame.DataContext = new PlayListManagerViewModel(user);
                frame.Source = uri;
            }
        }
    }
}
