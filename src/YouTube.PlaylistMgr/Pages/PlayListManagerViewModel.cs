﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using YouTube.PlaylistMgr.Commands;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;
using log4net;

namespace YouTube.PlaylistMgr.Pages
{
    public class PlayListManagerViewModel
    {
        private static ILog _logger = LogManager.GetLogger(typeof (PlayListManagerViewModel));

        public User User { get; set; }
        public LinkCollection Links { get; set; }
        public YoutubeServiceHelper YoutubeHelper { get; private set; }
        public ILocalClipboardService ClipboardService { get;  set; }

        public PlayListManagerViewModel(User user)
        {
            User = user;
            Links = new LinkCollection();

            YoutubeHelper = new YoutubeServiceHelper(User.RefreshToken);
            FillPlayLists();

        }
        public async void FillPlayLists()
        {
            var playLists = await YoutubeHelper.GetPlayLists(User.RefreshToken);
            foreach (var playlist in playLists.OrderBy(playlist => playlist.Snippet.Title))
            {
                var uri = playlist.Uri(User.RefreshToken);
                var link = new Link {DisplayName = playlist.Snippet.Title, Source = uri};
                Links.Add(link);
            }
        }

    }
}
