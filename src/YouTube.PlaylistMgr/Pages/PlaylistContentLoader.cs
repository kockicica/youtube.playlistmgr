﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using FirstFloor.ModernUI.Windows;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;

namespace YouTube.PlaylistMgr.Pages
{
    public class PlaylistContentLoader: DependencyObject,IContentLoader
    {
        public ILocalClipboardService ClipboardService { get; set; }


        public async Task<object> LoadContentAsync(Uri uri, CancellationToken cancellationToken)
        {
            string[] parts = uri.OriginalString.Split('?');
            var playlistContent = new PlayListContent();
            var helper = new YoutubeServiceHelper(parts[1]);
            var items = await helper.GetPlayList(parts[2]);
            playlistContent.DataContext = new PlayListContentViewModel(playlistContent, ClipboardService)
                {
                    Items = new ObservableCollection<PlaylistItem>(items),
                    Playlist = new Playlist{Id = parts[2]},
                    RefreshToken = parts[1],
                    YoutubeServiceHelper = helper
                };
            return playlistContent;
        }


    }
}
