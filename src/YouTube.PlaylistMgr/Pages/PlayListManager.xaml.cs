﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using YouTube.PlaylistMgr.Service;

namespace YouTube.PlaylistMgr.Pages
{
    /// <summary>
    /// Interaction logic for PlayListManager.xaml
    /// </summary>
    public partial class PlayListManager : UserControl
    {
        public PlayListManager()
        {
            InitializeComponent();
        }

        public PlayListManagerViewModel ViewModel
        {
            get { return DataContext as PlayListManagerViewModel; }
            set { DataContext = value; }
        }

        private void PlayLists_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Handled == false)
            {
                IList<PlaylistItem> droppedItems = null;
                if (e.Data.GetDataPresent("playlistitem"))
                {
                    droppedItems = e.Data.GetData("playlistitem") as IList<PlaylistItem>;
                }

                ModernTab tab = sender as ModernTab;
                var item = tab.InputHitTest(e.GetPosition(tab));
                if (item != null && item is TextBlock && droppedItems != null)
                {
                    var tst = item as TextBlock;
                    var link = ((ContentPresenter) tst.TemplatedParent).Content as Link;
                    if (link != null)
                    {
                        string[] tokens = link.Source.OriginalString.Split('?');
                        string token = tokens[1];
                        string playListId = tokens[2];
                        foreach (var droppedItem in droppedItems)
                        {
                            if (droppedItem.PlaylistId != playListId)
                            {
                                if (e.KeyStates == DragDropKeyStates.ControlKey && e.AllowedEffects.HasFlag(DragDropEffects.Copy))
                                {
                                    ViewModel.YoutubeHelper.CopyVideoToPlayList(playListId, droppedItem);
                                    e.Effects = DragDropEffects.Copy;
                                }
                                else if (e.AllowedEffects.HasFlag(DragDropEffects.Move))
                                {
                                    ViewModel.YoutubeHelper.CopyVideoToPlayList(playListId, droppedItem, true);
                                    var originatingListLink = tab.Links.SingleOrDefault(link1 => link1.IsLinkFor(ViewModel.User.RefreshToken,droppedItem.PlaylistId));
                                    if (originatingListLink != null)
                                    {
                                        originatingListLink.Source = originatingListLink.Source.NewUri();
                                        tab.SelectedSource = originatingListLink.Source;
                                    }
                                    e.Effects = DragDropEffects.Move;
                                }
                            }
                        }
                        link.Source = link.Source.NewUri();
                    }
                    e.Handled = true;
                }
            }
        }
    }
}
