﻿using System.Windows;
using FirstFloor.ModernUI.Presentation;
using YouTube.PlaylistMgr.Properties;
using log4net.Config;

namespace YouTube.PlaylistMgr
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            XmlConfigurator.Configure();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            AppearanceManager.Theme = Settings.Default.Theme == "dark" ? Theme.Dark : Theme.Light;
            AppearanceManager.AccentColor = Settings.Default.AccentColor;
        }
    }
}
