﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using YouTube.PlaylistMgr.Properties;

namespace YouTube.PlaylistMgr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Settings.Default.Save();
        }
    }
}
