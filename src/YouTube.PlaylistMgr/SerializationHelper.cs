﻿using System.IO;
using System.Xml.Serialization;

namespace YouTube.PlaylistMgr
{
    public class SerializationHelper
    {
        public static void Serialize<T>(T data, string fileName)
        {
            var ser = new XmlSerializer(typeof(T));
            using (var fstream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                ser.Serialize(fstream, data);
            }

        }

        public static T Deserialize<T>(string filename)
        {
            T readed = default(T);
            if (File.Exists(filename))
            {
                var ser = new XmlSerializer(typeof(T));
                using (var fstream = new FileStream(filename, FileMode.Open))
                {
                    readed = ((T)ser.Deserialize(fstream));
                }
            }
            return readed;
        }

        public static string SerializeToString<T>(T data)
        {
            var ser = new XmlSerializer(typeof(T));
            string ret = string.Empty;
            using (var sstream = new StringWriter())
            {
                ser.Serialize(sstream, data);
                sstream.Flush();
                ret = sstream.ToString();
            }
            return ret;

        }

        public static T DeserializeFromString<T>(string text)
        {
            T readed = default(T);
            var ser = new XmlSerializer(typeof(T));
            using (var sstream = new StringReader(text))
            {
                readed = (T)ser.Deserialize(sstream);
            }
            return readed;
        }

    }
}
