﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;
using log4net;
using Google.Apis.Util;

namespace YouTube.PlaylistMgr.Commands
{
    public class CopyPlaylistItemsCommand: ICommand
    {
        private static ILog _logger = LogManager.GetLogger(typeof (CopyPlaylistItemsCommand));
        private ILocalClipboardService _clipboardService;
        private PlayListContentViewModel _contentViewModel;

        public CopyPlaylistItemsCommand(ILocalClipboardService clipboardService, PlayListContentViewModel contentViewModel)
        {
            _clipboardService = clipboardService;
            _contentViewModel = contentViewModel;
        }

        public bool CanExecute(object parameter)
        {
            var items = parameter as IList;
            return (items != null && items.Count > 0);
        }

        public void Execute(object parameter)
        {
            var items = parameter as IList;
            if (items != null && items.Count > 0)
            {

                var properItems = new List<PlaylistItem>(items.Cast<PlaylistItem>());
                var str = SerializationHelper.SerializeToString(properItems);

                var data = new DataObject();
                data.SetData("playlists", str);
                data.SetData("operation", "copy");
                Clipboard.Clear();
                Clipboard.SetDataObject(data, false);
                _clipboardService.ItemsInCut.Clear();
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

    }
}
