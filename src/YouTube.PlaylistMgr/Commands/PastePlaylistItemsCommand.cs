﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;
using log4net;

namespace YouTube.PlaylistMgr.Commands
{
    public class PastePlaylistItemsCommand: ICommand
    {
        private static ILog _logger = LogManager.GetLogger(typeof (PastePlaylistItemsCommand));
        private ILocalClipboardService _localClipboard;
        private PlayListContentViewModel _contentViewModel;

        public PastePlaylistItemsCommand(ILocalClipboardService localClipboard, PlayListContentViewModel contentViewModel)
        {
            _localClipboard = localClipboard;
            _contentViewModel = contentViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return Clipboard.ContainsData("playlists");
        }

        public void Execute(object parameter)
        {
            var serializedItems = Clipboard.GetData("playlists");
            if (serializedItems != null)
            {
                try
                {
                    var items = SerializationHelper.DeserializeFromString<List<PlaylistItem>>((string)serializedItems);
                    if (items != null && items.Count > 0 && Clipboard.ContainsData("operation"))
                    {
                        string operation = (string)Clipboard.GetData("operation");
                        if (operation == "copy")
                        {
                            string targetPlayListId = _contentViewModel.Playlist.Id;
                            foreach (var playlistItem in items)
                            {
                                _contentViewModel.YoutubeServiceHelper.CopyVideoToPlayList(targetPlayListId, playlistItem, false);
                                _contentViewModel.Items.Add(playlistItem);
                            }
                        }
                        else
                        {
                            // cut
                            string targetPlayListId = _contentViewModel.Playlist.Id;
                            foreach (var playlistItem in items)
                            {
                                _contentViewModel.YoutubeServiceHelper.CopyVideoToPlayList(targetPlayListId, playlistItem, true);
                                _contentViewModel.Items.Add(playlistItem);
                            }
                        }
                        Clipboard.Clear();
                        _localClipboard.ItemsInCut.Clear();
                        
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
