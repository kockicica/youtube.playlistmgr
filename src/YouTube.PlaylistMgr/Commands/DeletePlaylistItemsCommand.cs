﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using FirstFloor.ModernUI.Windows.Controls;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;
using log4net;

namespace YouTube.PlaylistMgr.Commands
{
    class DeletePlaylistItemsCommand: ICommand
    {
        private static ILog _logger = LogManager.GetLogger(typeof (DeletePlaylistItemsCommand));

        private PlayListContentViewModel _viewModel;

        public DeletePlaylistItemsCommand(PlayListContentViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            var items = parameter as IList;
            return items.Count > 0;
        }

        public void Execute(object parameter)
        {
            var items = parameter as IList;
            if (items != null && items.Count > 0)
            {
                var result = ModernDialog.ShowMessage(string.Format("Do you REALLY want to delete {0} items from playlist?", items.Count), "Question",MessageBoxButton.YesNo);
                if (result.Value)
                {
                    var itemsToRemove = new List<PlaylistItem>();
                    try
                    {
                        foreach (var playlistItem in items.Cast<PlaylistItem>())
                        {
                            _viewModel.YoutubeServiceHelper.DeletePlaylistItem(playlistItem);
                            itemsToRemove.Add(playlistItem);
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e);
                    }
                    finally
                    {
                        itemsToRemove.ForEach(item => _viewModel.Items.Remove(item));
                    }
                }
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
