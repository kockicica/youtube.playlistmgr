﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using YouTube.PlaylistMgr.Content;
using YouTube.PlaylistMgr.Service;
using log4net;
using DotNetOpenAuth.Messaging;

namespace YouTube.PlaylistMgr.Commands
{
    public class CutPlaylistItemsCommand: ICommand
    {
        private static ILog _logger = LogManager.GetLogger(typeof (CutPlaylistItemsCommand));
        private ILocalClipboardService _localClipboard;
        private PlayListContentViewModel _contentViewModel;

        public CutPlaylistItemsCommand(ILocalClipboardService localClipboard, PlayListContentViewModel contentViewModel)
        {
            _localClipboard = localClipboard;
            _contentViewModel = contentViewModel;
        }

        public bool CanExecute(object parameter)
        {
            var items = parameter as IList;
            return (items != null && items.Count > 0);
        }

        public void Execute(object parameter)
        {
            var items = parameter as IList;
            if (items != null && items.Count > 0)
            {

                var properItems = new List<PlaylistItem>(items.Cast<PlaylistItem>());
                var str = SerializationHelper.SerializeToString(properItems);
                _localClipboard.ItemsInCut.Clear();
                _localClipboard.ItemsInCut.AddRange(properItems);
                var data = new DataObject();
                data.SetData("playlists", str);
                data.SetData("operation", "cut");
                Clipboard.Clear();
                Clipboard.SetDataObject(data, false);

            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        private List<PlaylistItem> GetCuttedPlaylistItemsOnClipboard()
        {
            var ret = new List<PlaylistItem>();
            if (Clipboard.ContainsData("playlists"))
            {
                if (Clipboard.ContainsData("operation"))
                {
                    var operation = (string)Clipboard.GetData("operation");
                    if (operation == "cut")
                    {
                        var serializedItems = Clipboard.GetData("playlists");
                        ret.AddRange(SerializationHelper.DeserializeFromString<List<PlaylistItem>>((string)serializedItems));
                    }
                }
            }
            return ret;
        }
    }
}